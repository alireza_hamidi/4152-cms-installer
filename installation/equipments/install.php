<?php
$dir  = $_POST["data"];
$cdir=json_decode($dir);
 
$dbInfo= $cdir->dbdetiles; 
$servername = $dbInfo->url;
$username = $dbInfo->username;
$password = $dbInfo->password;
$adminUsername= $cdir->adminDetiles->username;
$adminPassword= $cdir->adminDetiles->password;
$adminEmail = $cdir->adminDetiles->email;
$serverUrl=$cdir->domain;
$dbName = "4152_cms";
$tableName = "users";

$json_obj=array(
    "servername"=>$servername,
    "username"=>$username,
    "password"=>$password,
    "usersDbName"=>$dbName,
    "usersTable"=>$tableName
);
// $json_obj["servername"]=$servername;
// $json_obj["username"]=$username;
// $json_obj["password"]=$password;
// $json_obj["usersDbName"]=$dbName;
// $json_obj["usersTable"]=$tableName;
$encoded=json_encode($json_obj);

// echo $encoded;
$url = $serverUrl.'/resources/equipments/set_config.php';

// $ch = curl_init('http://webdev/4152-cms/resources/equipments/get_set_config.php');                                                                      


$ch = curl_init($url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
curl_setopt($ch, CURLOPT_POSTFIELDS, array("json"=>$encoded));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$result = curl_exec($ch);
curl_close($ch);
$json_result = json_decode($result, true);

if($result!="true"){ 
    echo "Error In Make Config File";
    return;
}

// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Create database
$sql = "CREATE DATABASE IF NOT EXISTS ".$dbName;

$response;
if ($conn->query($sql) === TRUE) {
    $response = "true";
} else {
    echo "Error creating database: " . $conn->error;
    return;
}

$conn->close();

$conn=new mysqli($servername, $username, $password,$dbName);

$sql = "CREATE TABLE ".$tableName." (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
username VARCHAR(15) NOT NULL,
password VARCHAR(30) NOT NULL,
email VARCHAR(50),
access VARCHAR(15),
sub_access VARCHAR(15),
profile json DEFAULT Null,
reg_date VARCHAR(25)
)";


if (mysqli_query($conn, $sql)) {
    $response = "true";
} else {
    echo "Error creating table: " . mysqli_error($conn);
    return;
}


$sql = "INSERT INTO ".$tableName." (username, password, email, access, sub_access,reg_date)
VALUES ('".$adminUsername."', '".$adminPassword."',' ".$adminEmail."','admin','base', '".date("Y-m-d")." ".date("h:i:sa")."')";

if ($conn->query($sql) === TRUE) {
    $response = "true";
} else {
    echo "Error: " . $sql . "<br>" . $conn->error;
    return;
}

mysqli_close($conn);

echo $response;
?> 