<?php

function getConfig(){
    $hatched=base64_encode("config.json");
    $bad = array_merge(
            array_map('chr', range(0,31)),
            array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
    $result = str_replace($bad, "", $hatched);

    $myfile = file_get_contents("../../equipments/".$result) or die("Unable to open file!");
    $decoded=base64_decode($myfile,true);
    return json_decode($decoded,true);
    }

function setConfig($data){
    $hatched=base64_encode("config.json");
    $bad = array_merge(
            array_map('chr', range(0,31)),
            array("<", ">", ":", '"', "/", "\\", "|", "?", "*"));
            
    $result = str_replace($bad, "", $hatched);
    file_put_contents("../../equipments/".$hatched,base64_encode(implode('->', $data)));
    return 'true';
}

?>